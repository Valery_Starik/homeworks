﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace HW_RegExp
{
    class Program
    {
        static void Main(string[] args)
        {            
            //Решение заданий из методички Глава2. Номера заданий: 1,2,3,4
            HomeWorkPart1();
            //Решение заданий из методички Глава2. Номера заданий: 7a-7e
            HomeWorkPart2();

        }

        public static void HomeWorkPart2()
        {
            var str = System.IO.File.ReadAllText("Text.txt");
            AllWorldsInStartString(str);
            AllWorldsInEndString(str);
            AllStartSpaceString(str);
            Console.ReadKey();
        }

        public static void HomeWorkPart1()
        {
            Console.WriteLine("Solution task 1-4");
            string str = DataString();
            AllWorlds(str);
            AllWorldsA(str);
            StartOfAEIOU(str);
            DoubleSum(str);
            Console.Clear();
            Console.ReadKey();
        }

        public static string DataString()
        {
            Console.WriteLine("Enter string: ");
            return Console.ReadLine();
        }

        public static void AllWorlds(string str)
        {
            string pattern = @"\S*";
            MatchCollection Matches = Regex.Matches(str, pattern, RegexOptions.IgnoreCase | RegexOptions.ExplicitCapture);
            Console.WriteLine("Write all worlds:");
            foreach (Match nextMatch in Matches)
            {
                string res = nextMatch.ToString();
                if (res != "")
                {
                    Console.WriteLine("res: " + res);
                }
            }
        }

        public static void AllWorldsInStartString(string str)
        {
            string pattern = @"^\s*\S*";
            MatchCollection Matches = Regex.Matches(str, pattern, RegexOptions.IgnoreCase | 
                RegexOptions.ExplicitCapture | RegexOptions.Multiline);
            Console.WriteLine("Write all worlds in start string:");
            foreach (Match nextMatch in Matches)
            {
                string res = nextMatch.ToString();
                if (res != "")
                {
                    Console.WriteLine("res: " + res);
                }
            }
        }

        public static void AllWorldsInEndString(string str)
        {
            string pattern = @"\S*\r";
            MatchCollection Matches = Regex.Matches(str, pattern, RegexOptions.IgnoreCase |
                RegexOptions.ExplicitCapture | RegexOptions.Multiline);
            Console.WriteLine("Write all worlds in end string:");
            foreach (Match nextMatch in Matches)
            {
                string res = nextMatch.ToString();
                if (res != "")
                {
                    Console.WriteLine("res: " + res);
                }
            }
        }

        public static void AllStartSpaceString(string str)
        {
            string pattern = @"^\s(\s*(\S*)){0,}";
            MatchCollection Matches = Regex.Matches(str, pattern, RegexOptions.IgnoreCase |
                RegexOptions.ExplicitCapture | RegexOptions.Multiline);
            Console.WriteLine("Write all worlds in end string:");
            foreach (Match nextMatch in Matches)
            {
                string res = nextMatch.ToString();
                if (res != "")
                {
                    Console.WriteLine("res: " + res);
                }
            }
        }
          

        public static void AllWorldsA(string str)
        {
            string pattern = @"\ba\S*a\b";
            MatchCollection Matches = Regex.Matches(str, pattern, RegexOptions.IgnoreCase | RegexOptions.ExplicitCapture);
            Console.WriteLine("Write all worlds start and end of 'a':");
            foreach (Match nextMatch in Matches)
            {
                string res = nextMatch.ToString();

                Console.WriteLine("res: " + res);

            }
        }

        public static void StartOfAEIOU(string str)
        {
            string pattern = @"\b\S*[aeiou]{2,4}\S*\B";
            MatchCollection Matches = Regex.Matches(str, pattern, RegexOptions.IgnoreCase | RegexOptions.ExplicitCapture);
            Console.WriteLine("Write result: ");
            foreach (Match nextMatch in Matches)
            {
                string res = nextMatch.ToString();

                Console.WriteLine("res: " + res);
            }
        }

        public static void DoubleSum(string str)
        {
            string pattern = @"\b\d,\d\b";
            MatchCollection Matches = Regex.Matches(str, pattern, RegexOptions.IgnoreCase | RegexOptions.ExplicitCapture);
            Console.WriteLine("Digit sum: ");
            double sum = 0;
            foreach (Match nextMatch in Matches)
            {
                string res = nextMatch.ToString();
                double a = Convert.ToDouble(res);
                sum += a;
            }
            Console.WriteLine("sum: " + sum);
        }

       

    }
}
