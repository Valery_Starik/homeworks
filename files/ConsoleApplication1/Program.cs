﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace ConsoleApplication1
{
    class Program
    {
        public static Dictionary<double, List<System.IO.FileInfo>> openWith =
             new Dictionary<double, List<System.IO.FileInfo>>();
        public static string startFolder = @"G:\Фотки катя\";
        public static int summ=0;

        private static void QueryFilesBySize()
        {
            var fileList = GetFiles(startFolder);
            var querySizeGroups =
            from file in fileList
            let len = GetFileLength(file)
            where len > 0
            group file by (len) into fileGroup
            where fileGroup.Count() > 1
            orderby fileGroup.Key descending
            select fileGroup;
            
            foreach (var filegroup in querySizeGroups)
            {
                Console.WriteLine(filegroup.Key.ToString() + " ");
                var f = new List<FileInfo>();

                foreach (var item in filegroup)
                {
                    f.Add(new FileInfo(item.FullName));
                    if (!openWith.ContainsKey(Convert.ToDouble(item.Length)))
                    {
                        openWith.Add(Convert.ToDouble(item.Length), f);
                    }
                    else
                    {
                        openWith[Convert.ToDouble(item.Length)] = f;
                    }
                    Console.WriteLine();

                    Console.WriteLine("\t{0}: {1}", item.FullName, item.Length);

                }

            }



        }

        public static void Dict()
        {
            foreach (var key in openWith.Keys)
            {
                //Console.WriteLine("Key "+key);
                List<FileInfo> File = openWith[key];
                //Console.WriteLine("count " + File.Count);

                for (int j = 0; j < File.Count; j++)
                {
                    for (int i = 1; i < File.Count; i++)
                    {
                        //if (CompareFilesCheck(File[j].ToString(), File[i].ToString()))
                            if (Comp(File[j].ToString(), File[i].ToString()))

                            {
                                if (i != j)
                                {
                                    summ++;
                                Console.WriteLine(File[j] + " with " + File[i]);
                                    Console.ForegroundColor = ConsoleColor.Green;
                                    Console.WriteLine("true");
                                    Console.ForegroundColor = ConsoleColor.Gray;
                                }
                            }
                        else
                        {
                            //Console.WriteLine("false");
                        }
                            }
                }
             }
        }
        

        public static bool CompareFilesCheck(string file1_path, string file2_path)
        {
                file1_path = startFolder + file1_path;
                file2_path = startFolder + file2_path;

            //StreamReader file1_sr = new StreamReader(file1_path);
            //StreamReader file2_sr = new StreamReader(file2_path);
            //bool a = false;
            //while (!file1_sr.EndOfStream)
            //{
            //    if (file1_sr.Read() == file2_sr.Read())
            //        a=true;
            //    if (file1_sr.Read() != file2_sr.Read())
            //        a=false;
            //    }
            //file1_sr.Dispose();
            //file2_sr.Dispose();
            //return a;




            Int32 currentByte;
            Int32 currentByte2;
            using (StreamReader reader = new StreamReader(file1_path))
            {
                currentByte2 = reader.BaseStream.ReadByte();
            }

            using (StreamReader reader = new StreamReader(file2_path))
            {
                currentByte = reader.BaseStream.ReadByte();
            }
            if (currentByte == currentByte2)
                return true;
            else return false;
        }


        public static bool Comp(string file1_path, string file2_path)
        {
            //file1_path = startFolder + file1_path;
            //file2_path = startFolder + file2_path;
            using (FileStream FirstFile = new FileStream(file1_path, FileMode.Open, FileAccess.Read), 
                SecondFile = new FileStream(file2_path, FileMode.Open, FileAccess.Read))
            {

                byte[] byte1 = new byte[1024];//1024
                byte[] byte2 = new byte[1024];

                int res1, res2;
                do
                {
                    res1 = FirstFile.Read(byte1, 0, byte1.Length);
                    res2 = SecondFile.Read(byte2, 0, byte2.Length);
                    {
                        for (int i = 0; i < byte1.Length; i++)
                        {
                            if (byte1[i] != byte2[i])
                            {
                                return false;
                            }
                        }
                    }
                }
                while (res1 != 0 && res2 != 0);

                FirstFile.Dispose();
                SecondFile.Dispose();
            }
            
            return true;

        }


        static long GetFileLength(System.IO.FileInfo fi)
        {
            long retval;
            try
            {
                retval = fi.Length;
            }
            catch (System.IO.FileNotFoundException)
            {
                retval = 0;
            }
            return retval;
        }

        static IEnumerable<System.IO.FileInfo> GetFiles(string path)
        {
            if (!System.IO.Directory.Exists(path))
                throw new System.IO.DirectoryNotFoundException();

            string[] fileNames = null;
            List<System.IO.FileInfo> files = new List<System.IO.FileInfo>();

            fileNames = System.IO.Directory.GetFiles(path, "*.*", System.IO.SearchOption.AllDirectories);
            foreach (string name in fileNames)
            {
                files.Add(new System.IO.FileInfo(name));
            }
            return files;
        }

        static void Main(string[] args)
        {
            TimeSpan time = new TimeSpan();
            DateTime time1 = new DateTime();
            DateTime time2 = new DateTime();
            time1 = DateTime.Now;
            QueryFilesBySize();
            Dict();
            time2 = DateTime.Now;
            time = time2 - time1;
            Console.ForegroundColor=ConsoleColor.Green;
            Console.WriteLine("Complete. Equals files is: " + summ+" Time: "+time.TotalSeconds);
            Console.ReadKey();
        }
    }
}
