﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF
{
    class Program
    {
        public static string Digit(int a)
        {
            return String.Format("Decimal: {0}, Binar: {1}, Hex: {2}",
                Convert.ToString(a),Convert.ToString(a,2), Convert.ToString(a,16));
        }

        public static string Pi()
        {
           return String.Format("Pi(5): {0:0.00000}, Pi(E): {0:E}", Math.PI);
        }

        public static void HelloWorld()
        {
            string str = "Hello World!";
            char[] b = str.ToCharArray();
            foreach (var a in b)
            {
                Console.WriteLine(String.Format("{0} = {1:x4}", a, (int)a));
            }
        }

        public static string DateCult()
        {
            DateTime d = DateTime.Now;
            string USA = d.ToString("D", new CultureInfo("en-US"));
            string Ukraine = d.ToString("D", new CultureInfo("uk-UA"));
            string Japan = d.ToString("D", new CultureInfo("ja-JP"));
            return String.Format("USA: {0}, Ukraine: {1}, Japan: {2}", USA, Ukraine, Japan);
        }


        static void Main(string[] args)
        {
            Console.WriteLine(Digit(345));
            Console.WriteLine(Pi());
            Console.WriteLine(DateCult());
            HelloWorld();
            Console.ReadKey();
        }
    }
}
